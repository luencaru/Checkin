import datetime
from django.utils import timezone
from django.utils.timezone import now
from rest_framework import serializers
from accounts.serializers import UserInOthersSerializers
from checks.models import CheckIn
from locals.serializers import RetriveEventSerializer
from schedules.serializers import RetrieveScheduleSerializer

__author__ = 'lucaru9'


class ListCreateCheckSerializer(serializers.ModelSerializer):
    check_out = serializers.BooleanField(read_only=True)
    hour_out = serializers.TimeField(read_only=True)
    date_out = serializers.DateField(read_only=True)
    user = serializers.IntegerField(source='user.id', read_only=True)

    class Meta:
        model = CheckIn
        fields = ('id', 'user', 'hour_in', 'date_in', 'schedule', 'image', 'check_out', 'hour_out', 'date_out')


class RetrieveCheckSerializer(serializers.ModelSerializer):
    user = UserInOthersSerializers(read_only=True)
    schedule = RetrieveScheduleSerializer(read_only=True)
    hour_in = serializers.TimeField(read_only=True)
    date_in = serializers.DateField(read_only=True)
    hour_out = serializers.TimeField(read_only=True)
    date_out = serializers.DateField(read_only=True)
    image = serializers.FileField(read_only=True)
    check_out = serializers.BooleanField(read_only=True)

    class Meta:
        model = CheckIn
        fields = ('id', 'user', 'hour_in', 'date_in', 'schedule', 'image', 'check_out', 'hour_out', 'date_out')

    def update(self, instance, validated_data):
        now_datetime = datetime.datetime.now()
        instance.check_out = True
        instance.hour_out = now_datetime.time()
        instance.date_out = now_datetime.date()
        instance.save()
        return instance
