from checks.views import CheckInAPI, CheckOutAPI, CheckInVerifiedAPI, OneCheckInVerifiedAPI

__author__ = 'lucaru9'
from django.conf.urls import url

urlpatterns = [
    url(r'^api/v1/checkins/$', CheckInAPI.as_view(), name='checkin-list-create'),
    url(r'^api/v1/checkins/verified/$', CheckInVerifiedAPI.as_view(), name='checkin-verified'),
    url(r'^api/v1/checkins/(?P<pk>\d+)/verified/$', OneCheckInVerifiedAPI.as_view(), name='checkin-one-create'),
    url(r'^api/v1/checkouts/(?P<pk>\d+)/retrieve/$', CheckOutAPI.as_view(), name='checkout-create'),
]
