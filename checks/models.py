from django.contrib.auth.models import User
from django.db import models


# Create your models here.
from locals.models import Event
from schedules.models import Schedule


class CheckIn(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User, related_name='checkins', verbose_name='Usuario')
    hour_in = models.TimeField(auto_now_add=True, verbose_name='Hora ingreso')
    date_in = models.DateField(auto_now_add=True, verbose_name='Dia ingreso')
    schedule = models.OneToOneField(Schedule, related_name='checkin', verbose_name='Horario')
    image = models.ImageField(upload_to='checkins/images/', null=True, blank=True, verbose_name='Imagen')
    check_out = models.BooleanField(default=False, verbose_name='CheckOut')
    hour_out = models.TimeField(auto_now=False, null=True, verbose_name='Hora salida')
    date_out = models.DateField(auto_now=False, null=True, verbose_name='Dia salida')

    class Meta:
        verbose_name_plural = "CheckIns"
        verbose_name = "CheckIn"
        ordering = ['-created_at']
        unique_together = ("user", "schedule")

    def __unicode__(self):
        return u'Usuario: {} en Evento:{} Fecha:{}'.format(self.user.get_full_name(), self.schedule.event.name,
                                                           self.date_in)
