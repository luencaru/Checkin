# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('schedules', '0001_initial'),
        ('checks', '0003_remove_checkin_event'),
    ]

    operations = [
        migrations.AddField(
            model_name='checkin',
            name='schedule',
            field=models.ForeignKey(related_name='checkins', verbose_name=b'Horario', to='schedules.Schedule', null=True),
        ),
    ]
