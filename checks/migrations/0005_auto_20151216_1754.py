# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('checks', '0004_checkin_schedule'),
    ]

    operations = [
        migrations.AlterField(
            model_name='checkin',
            name='schedule',
            field=models.ForeignKey(related_name='checkins', default=1, verbose_name=b'Horario', to='schedules.Schedule'),
            preserve_default=False,
        ),
    ]
