# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('checks', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='checkin',
            name='event',
            field=models.ForeignKey(related_name='checkins', verbose_name=b'Evento', to='locals.Event', null=True),
        ),
    ]
