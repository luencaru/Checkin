# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('checks', '0006_auto_20151217_0028'),
    ]

    operations = [
        migrations.AlterField(
            model_name='checkin',
            name='schedule',
            field=models.OneToOneField(related_name='checkin', verbose_name=b'Horario', to='schedules.Schedule'),
        ),
    ]
