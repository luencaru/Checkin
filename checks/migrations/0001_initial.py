# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('locals', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='CheckIn',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('hour_in', models.TimeField(auto_now_add=True, verbose_name=b'Hora ingreso')),
                ('date_in', models.DateField(auto_now_add=True, verbose_name=b'Dia ingreso')),
                ('image', models.ImageField(upload_to=b'checkins/images/', null=True, verbose_name=b'Imagen', blank=True)),
                ('check_out', models.BooleanField(default=False, verbose_name=b'CheckOut')),
                ('hour_out', models.TimeField(null=True, verbose_name=b'Hora salida')),
                ('date_out', models.DateField(null=True, verbose_name=b'Dia salida')),
                ('event', models.ForeignKey(related_name='checkins', verbose_name=b'Evento', to='locals.Event')),
                ('user', models.ForeignKey(related_name='checkins', verbose_name=b'Usuario', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['-created_at'],
                'verbose_name': 'CheckIn',
                'verbose_name_plural': 'CheckIns',
            },
        ),
    ]
