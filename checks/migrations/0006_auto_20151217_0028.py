# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('checks', '0005_auto_20151216_1754'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='checkin',
            unique_together=set([('user', 'schedule')]),
        ),
    ]
