# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('checks', '0002_auto_20151216_1739'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='checkin',
            name='event',
        ),
    ]
