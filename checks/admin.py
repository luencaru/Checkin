from django.contrib import admin
from daterange_filter.filter import DateRangeFilter
# Register your models here.
from import_export import resources, fields
from import_export.admin import ImportExportModelAdmin, ExportMixin
from checks.models import CheckIn
from import_export.widgets import ForeignKeyWidget, DateWidget, BooleanWidget


class CheckInResource(resources.ModelResource):
    user__username = fields.Field(column_name='Username', attribute='user__username')
    user__first_name = fields.Field(column_name='Nombres', attribute='user__first_name')
    user__last_name = fields.Field(column_name='Apellidos', attribute='user__last_name')
    user__userprofile__dni = fields.Field(column_name='Dni', attribute='user__userprofile__dni')
    date_in = fields.Field(column_name='Fecha ingreso', attribute='date_in')
    hour_in = fields.Field(column_name='hora ingreso', attribute='hour_in')
    event__name = fields.Field(column_name='Evento', attribute='event__name')
    check_out = fields.Field(column_name='Checkout', attribute='check_out', widget=BooleanWidget())
    date_out = fields.Field(column_name='Fecha salida', attribute='date_out')
    hour_out = fields.Field(column_name='hora salida', attribute='hour_out')

    class Meta:
        model = CheckIn
        fields = (
            'user__username', 'user__first_name', 'user__last_name', 'user__userprofile__dni', 'date_in', 'hour_in',
            'event__name', 'check_out', 'date_out', 'hour_out')
        export_order = (
            'user__username', 'user__first_name', 'user__last_name', 'user__userprofile__dni', 'date_in', 'hour_in',
            'event__name', 'check_out', 'date_out', 'hour_out')


class CheckInAdmin(ExportMixin, admin.ModelAdmin):
    resource_class = CheckInResource
    list_display = (
        'username', 'full_name', 'dni', 'date_in', 'hour_in', 'event_name',
        'check_out', 'date_out', 'hour_out')
    list_filter = (('date_in', DateRangeFilter), ('date_out', DateRangeFilter), 'user__username', 'user__first_name',
                   'user__last_name', 'user__userprofile__dni', 'hour_in', 'schedule', 'check_out', 'hour_out')
    search_fields = ('user__userprofile__dni', 'user__username')
    readonly_fields = ('date_in', 'hour_in')

    def username(self, obj):
        return '%s' % (obj.user.username)

    username.short_description = 'username'

    def full_name(self, obj):
        return '%s' % (obj.user.get_full_name())

    full_name.short_description = 'full_name'

    def dni(self, obj):
        return '%s' % (obj.user.userprofile.dni)

    dni.short_description = 'dni'

    def event_name(self, obj):
        if hasattr(obj, 'schedule') and hasattr(obj.schedule, 'event'):
            return '%s' % (obj.schedule.event.name)
        else:
            return 'No hay Horarios'

    event_name.short_description = 'evento'

    def has_add_permission(self, request):
        return False


admin.site.register(CheckIn, CheckInAdmin)
