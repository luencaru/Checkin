from rest_framework.pagination import PageNumberPagination

__author__ = 'lucaru9'


class TenSetPagination(PageNumberPagination):
    page_size = 10
