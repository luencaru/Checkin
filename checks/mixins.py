import datetime
from checks.models import CheckIn

__author__ = 'lucaru9'


class SearchCheckInDate(object):
    def get_queryset(self):
        if self.request.GET.has_key('q'):
            q = datetime.datetime.strptime(self.request.GET.get('q'), "%Y-%m-%d").date()
            return self.request.user.checkins.filter(date_in=q).order_by('-last_modified')
        else:
            return self.request.user.checkins.all().order_by('-last_modified')
