from django.shortcuts import render, get_object_or_404


# Create your views here.
from rest_framework import status
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateAPIView, RetrieveAPIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from accounts.authentication import ExpiringTokenAuthentication
from checks.mixins import SearchCheckInDate
from checks.models import CheckIn
from checks.pagination import TenSetPagination
from checks.serializers import ListCreateCheckSerializer, RetrieveCheckSerializer
from schedules.models import Schedule


class CheckInAPI(SearchCheckInDate, ListCreateAPIView):
    serializer_class = ListCreateCheckSerializer
    authentication_classes = (ExpiringTokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    pagination_class = TenSetPagination
    # queryset = CheckIn.objects.all()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        obj = self.perform_create(serializer)
        serializer = RetrieveCheckSerializer(obj)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer):
        obj = serializer.save(user=self.request.user)
        return obj

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = RetrieveCheckSerializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = RetrieveCheckSerializer(queryset, many=True)
        return Response(serializer.data)


class CheckOutAPI(RetrieveUpdateAPIView):
    # authentication_classes = ()
    # permission_classes = (AllowAny,)
    authentication_classes = (ExpiringTokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = RetrieveCheckSerializer

    def get_object(self):
        obj = get_object_or_404(CheckIn, id=self.kwargs.get('pk'))
        return obj

    def perform_update(self, serializer):
        if not self.get_object().check_out:
            serializer.save()


class CheckInVerifiedAPI(APIView):
    authentication_classes = (ExpiringTokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    # authentication_classes = ()
    # permission_classes = (AllowAny,)

    def get(self, request, *args, **kwargs):
        query = CheckIn.objects.filter(check_out=False)
        if query.count() > 0:
            checkin = query.first()
            serializer = RetrieveCheckSerializer(checkin)
            return Response({"detail": "Tiene un CheckOut pendiente", "Checkin": serializer.data},
                            status=status.HTTP_302_FOUND)
        else:
            return Response({"detail": "Puede realizer CheckIn"}, status=status.HTTP_200_OK)


class OneCheckInVerifiedAPI(APIView):
    authentication_classes = (ExpiringTokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    # authentication_classes = ()
    # permission_classes = (AllowAny,)

    def get(self, request, *args, **kwargs):
        schedule = get_object_or_404(Schedule, id=self.kwargs.get('pk'))
        if hasattr(schedule, 'checkin'):
            serializer = RetrieveCheckSerializer(schedule.checkin)
            if schedule.checkin.check_out:
                return Response(serializer.data,
                                status=status.HTTP_200_OK)
            else:
                return Response(serializer.data,
                                status=status.HTTP_200_OK)
        else:
            return Response({"detail": "Todavia no realiza el CheckIn"},
                            status=status.HTTP_406_NOT_ACCEPTABLE)
