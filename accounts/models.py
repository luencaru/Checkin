# coding=utf-8
from django.contrib.auth.models import User
from django.db import models


# Create your models here.
class UserProfile(models.Model):
    MALE = "m"
    FEMALE = "f"
    created_at = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    user = models.OneToOneField(User, related_name='userprofile')
    address = models.CharField(max_length=400, verbose_name='Dirección', blank=True, null=True)
    dni = models.CharField(max_length=9, verbose_name='Dni', blank=True, null=True)
    gender = models.CharField(max_length=1, verbose_name='Sexo', blank=True, choices=(
        (MALE, "male"), (FEMALE, "female")
    ))

    class Meta:
        verbose_name_plural = "Perfiles"
        verbose_name = "Perfil"
        ordering = ['created_at']

    def __unicode__(self):
        return self.user.get_full_name()
