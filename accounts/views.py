from django.contrib.auth.models import User
from django.db import transaction
from django.shortcuts import render, get_object_or_404


# Create your views here.
from django.utils.decorators import method_decorator
from rest_framework import status, serializers
from rest_framework.generics import CreateAPIView, RetrieveUpdateDestroyAPIView, ListCreateAPIView, \
    RetrieveDestroyAPIView, UpdateAPIView, RetrieveAPIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from accounts.authentication import ExpiringTokenAuthentication
from accounts.mixins import TokenMixin, RefreshTokenMixin
from accounts.models import UserProfile
from accounts.serializers import LoginSerializer, RegisterUserSerializer, ChangePasswordSerializer, \
    RetrieveDestroyUserSerializer, UpdateUserSerializer


class LoginAPI(TokenMixin, APIView):
    def post(self, request, *args, **kwargs):
        serializer = LoginSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.get_user()
        token = self.regenerate(user)
        return Response({'token': token.key}, status=status.HTTP_200_OK)


class ChangePassAPI(APIView):
    authentication_classes = (ExpiringTokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def put(self, request, *args, **kwargs):
        serializer = ChangePasswordSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        if serializer.data.get("email") != request.user.email:
            raise serializers.ValidationError({"email": "Email mismatch"})
        if not request.user.check_password(serializer.data.get("old_password")):
            raise serializers.ValidationError({"password": "Password mismatch"})
        user = self.request.user
        user.set_password(serializer.validated_data.get("new_password"))
        user.save()
        return Response(status=status.HTTP_200_OK)


class UserRegisterAPI(RefreshTokenMixin, ListCreateAPIView):
    serializer_class = RegisterUserSerializer
    authentication_classes = (ExpiringTokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    # authentication_classes = ()
    # permission_classes = (AllowAny,)
    queryset = User.objects.filter(is_staff=False, is_superuser=False, is_active=True)

    @method_decorator(transaction.atomic)
    def dispatch(self, request, *args, **kwargs):
        return super(UserRegisterAPI, self).dispatch(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        if not User.objects.filter(username=serializer.validated_data.get('email')).exists():
            self.perform_create(serializer)
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
        else:
            return Response({"detail": 'Correo ya resgistrado'}, status=status.HTTP_303_SEE_OTHER)


class RetrieveDestroyUserAPI(RetrieveDestroyAPIView):
    serializer_class = RetrieveDestroyUserSerializer
    authentication_classes = (ExpiringTokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    # authentication_classes = ()
    # permission_classes = (AllowAny,)

    def get_object(self):
        obj = get_object_or_404(User, id=self.kwargs.get('pk'))
        return obj


class RetrieveUserAPI(RetrieveAPIView):
    serializer_class = RetrieveDestroyUserSerializer
    authentication_classes = (ExpiringTokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    # authentication_classes = ()
    # permission_classes = (AllowAny,)

    def get_object(self):
        return self.request.user


class UpdateUserAPI(UpdateAPIView):
    serializer_class = UpdateUserSerializer
    authentication_classes = (ExpiringTokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    # authentication_classes = ()
    # permission_classes = (AllowAny,)

    def get_object(self):
        obj = get_object_or_404(User, id=self.kwargs.get('pk'))
        return obj
