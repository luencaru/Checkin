from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from rest_framework import serializers
from accounts.models import UserProfile

__author__ = 'lucaru9'


class LoginSerializer(serializers.Serializer):
    username = serializers.CharField(error_messages={"blank": "Este campo es obligatorio"})
    password = serializers.CharField(error_messages={"blank": "Este campo es obligatorio"})

    def validate(self, attrs):
        self.user_cache = authenticate(username=attrs["username"], password=attrs["password"])
        if not self.user_cache:
            raise serializers.ValidationError("Invalid login")
        if self.user_cache.is_active:
            return attrs
        else:
            raise serializers.ValidationError("Account Disabled")

    def get_user(self):
        return self.user_cache


class ChangePasswordSerializer(serializers.Serializer):
    old_password = serializers.CharField(error_messages={"blank": "Este campo es obligatorio"})
    new_password = serializers.CharField(error_messages={"blank": "Este campo es obligatorio"})
    email = serializers.EmailField(error_messages={"blank": "Este campo es obligatorio"})

    def validate(self, attrs):
        username = attrs.get('email')
        password = attrs.get('old_password')

        if username and password:
            user = authenticate(username=username, password=password)

            if user:
                if not user.is_active:
                    msg = 'User account is disabled.'
                    raise serializers.ValidationError(msg)
                attrs['user'] = user
                return attrs
            else:
                msg = 'Unable to log in with provided credentials.'
                raise serializers.ValidationError(msg)
        else:
            msg = 'Must include "username" and "password"'
            raise serializers.ValidationError(msg)


class RegisterProfileSerializer(serializers.ModelSerializer):
    dni = serializers.CharField(allow_blank=True, allow_null=True, required=False)
    address = serializers.CharField(allow_null=True, allow_blank=True, required=False)

    class Meta:
        model = UserProfile
        fields = ('dni', 'address', 'gender')


class RegisterUserSerializer(serializers.ModelSerializer):
    userprofile = RegisterProfileSerializer()

    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'email', 'userprofile')

    def create(self, validated_data):
        user = User(username=validated_data.get('email'), first_name=validated_data.get('first_name'),
                    last_name=validated_data.get('last_name'), email=validated_data.get('email'))
        user.set_password('{}{}'.format(validated_data.get('last_name'), validated_data.get('first_name')))
        user.save()
        profile = UserProfile(user=user, dni=validated_data.get('userprofile').get('dni', ''),
                              gender=validated_data.get('userprofile').get('gender'),
                              address=validated_data.get('userprofile').get('address', ''))
        profile.save()
        return user


class UpdateProfileSerializer(serializers.ModelSerializer):
    dni = serializers.CharField(required=False)
    address = serializers.CharField(required=False)
    gender = serializers.CharField(required=False)

    class Meta:
        model = UserProfile
        fields = ('dni', 'address', 'gender')


class RetrieveDestroyUserSerializer(serializers.ModelSerializer):
    dni = serializers.CharField(required=False, source='userprofile.dni', read_only=True)
    address = serializers.CharField(required=False, source='userprofile.address', read_only=True)
    gender = serializers.CharField(required=False, source='userprofile.gender', read_only=True)

    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'email', 'is_active', 'is_staff', 'dni', 'address', 'gender')


class UpdateUserSerializer(serializers.ModelSerializer):
    userprofile = UpdateProfileSerializer(required=False)

    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'is_active', 'is_staff', 'email', 'userprofile')

    def update(self, instance, validated_data):
        instance.first_name = validated_data.get('first_name', instance.first_name)
        instance.last_name = validated_data.get('last_name', instance.last_name)
        instance.email = validated_data.get('email', instance.email)
        instance.is_active = validated_data.get('is_active', instance.is_active)
        instance.is_staff = validated_data.get('is_staff', instance.is_staff)
        instance.save()
        if validated_data.get('userprofile'):
            instance.userprofile.gender = validated_data.get('userprofile').get('gender', instance.userprofile.gender)
            instance.userprofile.dni = validated_data.get('userprofile').get('dni', instance.userprofile.dni)
            instance.userprofile.address = validated_data.get('userprofile').get('address',
                                                                                 instance.userprofile.address)
            instance.userprofile.save()
        return instance


class UserProfileOtherSerializer(serializers.ModelSerializer):
    userprofile = UpdateProfileSerializer(read_only=True)

    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'username', 'userprofile')


class UserInOthersSerializers(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'username')
