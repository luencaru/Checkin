__author__ = 'lucaru9'

from django.apps import AppConfig


class ProfilesConfig(AppConfig):
    name = 'accounts'
    verbose_name = 'Perfiles'
