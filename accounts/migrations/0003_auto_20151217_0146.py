# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0002_auto_20151118_0315'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='userprofile',
            options={'ordering': ['created_at'], 'verbose_name': 'Perfil', 'verbose_name_plural': 'Perfiles'},
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='address',
            field=models.CharField(max_length=400, null=True, verbose_name=b'Direcci\xc3\xb3n', blank=True),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='dni',
            field=models.CharField(max_length=9, null=True, verbose_name=b'Dni', blank=True),
        ),
    ]
