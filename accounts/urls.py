from accounts.views import LoginAPI, UserRegisterAPI, ChangePassAPI, RetrieveDestroyUserAPI, UpdateUserAPI, \
    RetrieveUserAPI

__author__ = 'lucaru9'

from django.conf.urls import url

urlpatterns = [
    url(r'^api/v1/login/$', LoginAPI.as_view(), name='login'),
    url(r'^api/v1/users/register/$', UserRegisterAPI.as_view(), name='user-register'),
    url(r'^api/v1/users/retrieve/$', RetrieveUserAPI.as_view(), name='user-ret'),
    url(r'^api/v1/users/change-password/$', ChangePassAPI.as_view(), name='user-change-password'),
    url(r'^api/v1/users/(?P<pk>\d+)/retrieve/$', RetrieveDestroyUserAPI.as_view(), name='user-ret-del'),
    url(r'^api/v1/users/(?P<pk>\d+)/update/$', UpdateUserAPI.as_view(), name='user-updatee'),
]
