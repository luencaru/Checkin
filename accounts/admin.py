from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as AuthUserAdmin
# Register your models here.
from django.contrib.auth.models import User
from accounts.models import UserProfile

admin.site.register(UserProfile)


class UserProfileInline(admin.TabularInline):
    model = UserProfile
    max_num = 1
    min_num = 1
    can_delete = False


class UserAdmin(AuthUserAdmin):
    list_display = ('username', 'email', 'first_name', 'last_name', 'is_active', 'is_staff', 'is_superuser')
    inlines = [UserProfileInline]
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (('Personal info'), {'fields': ('first_name', 'last_name', 'email')}),
        (('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                      'groups', 'user_permissions')}),
        (('Important dates'), {'fields': ('last_login', 'date_joined')}),

    )

    def type(self, obj):
        return obj.userprofile.type


admin.site.unregister(User)
admin.site.register(User, UserAdmin)
