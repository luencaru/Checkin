from django.contrib.auth.models import Group
from django.shortcuts import render, get_object_or_404

# Create your views here.
from rest_framework.generics import CreateAPIView, ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from accounts.authentication import ExpiringTokenAuthentication
from groups.serializers import ListCreateGroupSerializer


class ListCreateGroupAPI(ListCreateAPIView):
    serializer_class = ListCreateGroupSerializer
    authentication_classes = (ExpiringTokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    queryset = Group.objects.all().order_by('name')


class RetrieveUpdateDestroyGroupAPI(RetrieveUpdateDestroyAPIView):
    serializer_class = ListCreateGroupSerializer
    authentication_classes = (ExpiringTokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    # authentication_classes = ()
    # permission_classes = (AllowAny,)

    def get_object(self):
        obj = get_object_or_404(Group, id=self.kwargs.get('pk'))
        return obj
