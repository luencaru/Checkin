from groups.views import ListCreateGroupAPI, RetrieveUpdateDestroyGroupAPI

__author__ = 'lucaru9'
from django.conf.urls import url

urlpatterns = [
    url(r'^api/v1/groups/create/$', ListCreateGroupAPI.as_view(), name='groups-list-create'),
    url(r'^api/v1/groups/(?P<pk>\d+)/retrieve/$', RetrieveUpdateDestroyGroupAPI.as_view(), name='groups-ret-del-upd'),

]
