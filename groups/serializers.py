from django.contrib.auth.models import Group
from rest_framework import serializers

__author__ = 'lucaru9'


class ListCreateGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ('id', 'name',)
