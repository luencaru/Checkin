from django.contrib import admin
from django.contrib.auth.admin import GroupAdmin


# Register your models here.
from django.contrib.auth.models import User, Group


class UserSetInline(admin.TabularInline):
    model = User.groups.through
    # raw_id_fields = ('user',)
    extra = 0
    can_delete = False
    verbose_name = 'Usuario'
    verbose_name_plural = 'Usuarios'


class GroupUserAdmin(GroupAdmin):
    list_display = ('name',)
    inlines = [UserSetInline]


admin.site.unregister(Group)
admin.site.register(Group, GroupUserAdmin)
