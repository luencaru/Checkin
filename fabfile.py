from __future__ import print_function
from fabric.api import *
from fabric.colors import green, red

env.colorize_errors = True


def server():
    env.user = 'ubuntu'
    env.host_string = '159.203.73.54'
    env.password = 'kodevianapp@2015'


def deploy():
    server()
    home_path = "/home/ubuntu"
    print(green("Beginning Deploy:"))
    with cd("{}/checkin".format(home_path)):
        run("git pull ")
        run("source {}/checkinEnv/bin/activate && pip install -r requirements.txt".format(home_path))
        run("source {}/checkinEnv/bin/activate"
            "&& python manage.py collectstatic --noinput --settings='checkin.settings.production'".format(
            home_path))
        run("source {}/checkinEnv/bin/activate"
            "&& python manage.py migrate --settings='checkin.settings.production'".format(home_path))
        sudo("service nginx restart", pty=False)
        sudo("supervisorctl restart gunicorn_checkin", pty=False)
