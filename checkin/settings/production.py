from .base import *
__author__ = 'lucaru9'
DEBUG = False

TEMPLATE_DEBUG = False

ALLOWED_HOSTS = ['checkin.kodevianapps.com']

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'checkinDB',
        'USER': 'postgres',
        'PASSWORD': 'postgreskode',
        'HOST': 'localhost'
    }
}

ADMINS = (
    ('Jose Manuel', 'manuel@kodevian.com'),
)
