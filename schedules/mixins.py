import datetime

__author__ = 'lucaru9'


class SearchScheduleDate(object):
    def get_queryset(self):
        if self.request.GET.has_key('q'):
            q = datetime.datetime.strptime(self.request.GET.get('q'), "%Y-%m-%d").date()
            return self.request.user.schedules.filter(is_enabled=True, date=q).order_by('-date', '-hour')
        else:
            return self.request.user.schedules.filter(is_enabled=True).order_by('-date', '-hour')
