from django.shortcuts import render

# Create your views here.
from rest_framework.generics import ListAPIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from accounts.authentication import ExpiringTokenAuthentication
from schedules.mixins import SearchScheduleDate
from schedules.pagination import TenSetPagination
from schedules.serializers import ListScheduleSerializer


class ListSchedulesAPI(SearchScheduleDate, ListAPIView):
    serializer_class = ListScheduleSerializer
    authentication_classes = (ExpiringTokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    pagination_class = TenSetPagination