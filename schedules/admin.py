from django.contrib import admin

# Register your models here.
from schedules.models import Schedule


class ScheduleAdmin(admin.ModelAdmin):
    model = Schedule
    list_display = ('user', 'date', 'hour', 'event', 'is_enabled')
    # radio_fields = {"event": admin.HORIZONTAL}


admin.site.register(Schedule, ScheduleAdmin)
