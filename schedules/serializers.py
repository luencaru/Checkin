from rest_framework import serializers
from accounts.serializers import UserProfileOtherSerializer
from locals.serializers import RetriveEventSerializer
from schedules.models import Schedule

__author__ = 'lucaru9'


class ListScheduleSerializer(serializers.ModelSerializer):
    user = UserProfileOtherSerializer(read_only=True)
    event = RetriveEventSerializer(read_only=True)

    class Meta:
        model = Schedule
        fields = ('id', 'user', 'hour', 'date', 'hour', 'event', 'is_enabled')


class RetrieveScheduleSerializer(serializers.ModelSerializer):
    event = RetriveEventSerializer(read_only=True)

    class Meta:
        model = Schedule
        fields = ('id', 'hour', 'date', 'hour', 'event', 'is_enabled')
