from schedules.views import ListSchedulesAPI

__author__ = 'lucaru9'
from django.conf.urls import url

urlpatterns = [
    url(r'^api/v1/schedules/$', ListSchedulesAPI.as_view(), name='schedules-list'),
]
