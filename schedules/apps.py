__author__ = 'lucaru9'

from django.apps import AppConfig


class SchedulesConfig(AppConfig):
    name = 'schedules'
    verbose_name = 'Horarios'