# encoding= utf-8
from django.contrib.auth.models import User
from django.db import models


# Create your models here.
from locals.models import Event


class Schedule(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User, related_name='schedules', verbose_name='Usuario')
    hour = models.TimeField(verbose_name='Hora')
    date = models.DateField(verbose_name='Día')
    event = models.ForeignKey(Event, related_name='schedules', verbose_name='Evento')
    is_enabled = models.BooleanField(default=True, verbose_name='Habilitar', help_text='Activar o descativar horario')

    class Meta:
        verbose_name_plural = "Horarios"
        verbose_name = "Horario"
        ordering = ['-date']

    def __unicode__(self):
        return u'{} {}'.format(self.user.get_full_name(), self.date, self.hour)
