# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('locals', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Schedule',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('hour', models.TimeField(verbose_name=b'Hora')),
                ('date', models.DateField(verbose_name=b'D\xc3\xada')),
                ('is_enabled', models.BooleanField(default=True, help_text=b'Activar o descativar horario', verbose_name=b'Habilitar')),
                ('event', models.ForeignKey(related_name='schedules', verbose_name=b'Evento', to='locals.Event')),
                ('user', models.ForeignKey(related_name='schedules', verbose_name=b'Usuario', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['date'],
                'verbose_name': 'Horario',
                'verbose_name_plural': 'Horarios',
            },
        ),
    ]
