from django.contrib import admin

# Register your models here.
from django.contrib.gis.admin import OSMGeoAdmin
from locals.models import Event


class EventAdmin(OSMGeoAdmin):
    default_zoom = 1
    readonly_fields = ('latitude', 'longitude')
    list_display = ('name', 'address', 'is_enabled')


admin.site.register(Event, EventAdmin)
