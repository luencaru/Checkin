from rest_framework import serializers
from locals.models import Event

__author__ = 'lucaru9'


class ListEventSerializer(serializers.ModelSerializer):
    class Meta:
        model = Event
        fields = ('id', 'name', 'address', 'longitude', 'latitude', 'is_enabled', 'file_1',
                  'description')


class RetriveEventSerializer(serializers.ModelSerializer):
    class Meta:
        model = Event
        fields = ('id', 'name', 'address', 'longitude', 'latitude', 'is_enabled', 'file_1',
                  'description')
