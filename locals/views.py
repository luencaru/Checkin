from django.shortcuts import render, get_object_or_404


# Create your views here.
from rest_framework.generics import ListCreateAPIView, ListAPIView, RetrieveAPIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from accounts.authentication import ExpiringTokenAuthentication
from locals.models import Event
from locals.serializers import ListEventSerializer, RetriveEventSerializer


class ListEventAPI(ListAPIView):
    serializer_class = ListEventSerializer
    authentication_classes = (ExpiringTokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    # authentication_classes = ()
    # permission_classes = (AllowAny,)
    queryset = Event.objects.filter(is_enabled=True)


class RetrieveEventAPI(RetrieveAPIView):
    serializer_class = RetriveEventSerializer
    authentication_classes = (ExpiringTokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    # authentication_classes = ()
    # permission_classes = (AllowAny,)

    def get_object(self):
        obj = get_object_or_404(Event, id=self.kwargs.get('pk'))
        return obj
