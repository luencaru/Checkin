# coding=utf-8
from django.contrib.gis.db import models


# Create your models here.
class Event(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=300, verbose_name='Nombre')
    address = models.CharField(max_length=500, verbose_name='Dirección')
    longitude = models.DecimalField(max_digits=10, decimal_places=7, verbose_name='Longitud', null=True, blank=True)
    latitude = models.DecimalField(max_digits=10, decimal_places=7, verbose_name='Latitud', null=True, blank=True)
    is_enabled = models.BooleanField(default=True, verbose_name='Habilitado',
                                     help_text='Habilitar o deshabilitar Evento')
    file_1 = models.FileField(upload_to='events/file', null=True, blank=True)
    # file_2 = models.FileField(upload_to='events/file', null=True, blank=True)
    # file_3 = models.FileField(upload_to='events/file', null=True, blank=True)
    location = models.PointField(u"longitude/latitude", geography=True, blank=True, null=True)
    description = models.TextField(null=True, blank=True)

    class Meta:
        verbose_name_plural = "Eventos"
        verbose_name = "Evento"
        ordering = ['name']

    def __unicode__(self):
        return u'{}'.format(self.name)

    def save(self, *args, **kwargs):
        if hasattr(self.location, 'y') and hasattr(self.location, 'x'):
            self.latitude = self.location.y
            self.longitude = self.location.x
        else:
            self.latitude = 0.0
            self.longitude = 0.0
        super(Event, self).save(*args, **kwargs)
