from locals.views import ListEventAPI, RetrieveEventAPI

__author__ = 'lucaru9'
from django.conf.urls import url

urlpatterns = [
    url(r'^api/v1/events/$', ListEventAPI.as_view(), name='event-list'),
    url(r'^api/v1/events/(?P<pk>\d+)/retrieve/$', RetrieveEventAPI.as_view(), name='event-retrieve'),

]
