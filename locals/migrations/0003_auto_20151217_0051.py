# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('locals', '0002_event_description'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='event',
            name='file_2',
        ),
        migrations.RemoveField(
            model_name='event',
            name='file_3',
        ),
    ]
