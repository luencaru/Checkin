# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(max_length=300, verbose_name=b'Nombre')),
                ('address', models.CharField(max_length=500, verbose_name=b'Direcci\xc3\xb3n')),
                ('longitude', models.DecimalField(null=True, verbose_name=b'Longitud', max_digits=10, decimal_places=7, blank=True)),
                ('latitude', models.DecimalField(null=True, verbose_name=b'Latitud', max_digits=10, decimal_places=7, blank=True)),
                ('is_enabled', models.BooleanField(default=True, help_text=b'Habilitar o deshabilitar Evento', verbose_name=b'Habilitado')),
                ('file_1', models.FileField(null=True, upload_to=b'events/file', blank=True)),
                ('file_2', models.FileField(null=True, upload_to=b'events/file', blank=True)),
                ('file_3', models.FileField(null=True, upload_to=b'events/file', blank=True)),
                ('location', django.contrib.gis.db.models.fields.PointField(srid=4326, geography=True, null=True, verbose_name='longitude/latitude', blank=True)),
            ],
            options={
                'ordering': ['name'],
                'verbose_name': 'Evento',
                'verbose_name_plural': 'Eventos',
            },
        ),
    ]
